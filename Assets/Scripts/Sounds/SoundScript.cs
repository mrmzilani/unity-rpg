﻿using UnityEngine;
using System.Collections;

public class SoundScript : MonoBehaviour {



	public static bool created = false;
	
	public AudioClip selectSound;
	
	
	void Awake () {
		//http://answers.unity3d.com/questions/34185/dontdestroyonload-is-it-intended-behavior.html
		
		
		if (!created) 
		{
			DontDestroyOnLoad (transform.gameObject);
			created = true;
			
		} 
		else 
		{
			Destroy(this.gameObject); 
		}
		
		
		
	}



	// Use this for initialization
	void Start () {
	
	
		
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	
	public void playSelectSound()
	{
		audio.clip =selectSound;
		audio.Play();
	}
	
}
