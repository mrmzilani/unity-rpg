using UnityEngine;
using System.Collections;

public class CreateNewCharacter : MonoBehaviour {


	

	private BasePlayer newPlayer;

	public static bool isMageClass;
	public static bool isWarriorClass;

	private string playerName = "Hero"; 

	bool selection;





	// Use this for initialization
	void Start () {
	
		newPlayer = new BasePlayer ();
		CreateNewCharacter.isWarriorClass = false;
		CreateNewCharacter.isMageClass = true;


	}


	void OnGUI()
	{

		playerName = GUILayout.TextArea (playerName,8);


		if(isWarriorClass = GUILayout.Toggle (isWarriorClass, "Warrior Class"))
		{
			isMageClass = false;
			
			GUI.Box(new Rect(200,0,200,32), "  HP: 50 / MP: 5 ");
			GUI.Box(new Rect(200,32,200,32),"Level: 1");
			GUI.Box(new Rect(200,64,200,32), "Strength: 10");
			GUI.Box(new Rect(200,96,200,32), "Intellect: 1");
			GUI.Box(new Rect(200,128,200,32),"Speed: 10");
			GUI.Box(new Rect(200,160,200,32), "Luck: 1");
		}
	

		if(isMageClass = GUILayout.Toggle (isMageClass, "Mage Class"))
		{
			isWarriorClass = false;
			
			GUI.Box(new Rect(200,0,200,32), "  HP: 30 / MP: 15 ");
			GUI.Box(new Rect(200,32,200,32),"Level: 1");
			GUI.Box(new Rect(200,64,200,32), "Strength: 1");
			GUI.Box(new Rect(200,96,200,32), "Intellect: 10");
			GUI.Box(new Rect(200,128,200,32),"Speed: 3");
			GUI.Box(new Rect(200,160,200,32), "Luck: 1");
			
			
		}

		
		

		if (GUILayout.Button ("Create")) 
		{

			if (isMageClass == true || isWarriorClass == true)
			{

					if (isMageClass)
					{

						newPlayer.thePlayerClass = new BaseMageClass ();

					}

					if (isWarriorClass)
					{
						newPlayer.thePlayerClass = new BaseWarriorClass ();
					}


						newPlayer.thePlayerName = playerName;

						newPlayer.thePlayerLevel = 1;
						newPlayer.theStamina = newPlayer.thePlayerClass.theStamina;
						newPlayer.theMaxStamina = newPlayer.thePlayerClass.theMaxStamina;
						newPlayer.theMP = newPlayer.thePlayerClass.theMP;
						newPlayer.theStrength = newPlayer.thePlayerClass.theStrength;
						newPlayer.theIntellect = newPlayer.thePlayerClass.theIntellect;
						newPlayer.theSpeed = newPlayer.thePlayerClass.theSpeed;
						newPlayer.theLuck = newPlayer.thePlayerClass.theLuck;
						
						newPlayer.theGold = newPlayer.thePlayerClass.theGold;
						
						storeNewPlayerInfo();
						SaveInformation.SaveAllInfomation ();

						Debug.Log ("Player Name: " + newPlayer.thePlayerName);
						Debug.Log ("Player Class: " + newPlayer.thePlayerClass.theclassName);

						Debug.Log ("Player Level: " + newPlayer.thePlayerLevel);
						Debug.Log ("Player Stamina: " + newPlayer.theStamina);
						Debug.Log ("Player Maxium Stamina: " + newPlayer.theMaxStamina);
						Debug.Log ("Player MP : " + newPlayer.theMP);
						Debug.Log ("Player Strength : " + newPlayer.theStrength);
						Debug.Log ("Player Intellect : " + newPlayer.theIntellect);
						Debug.Log ("Player Speed : " + newPlayer.theSpeed);
						Debug.Log ("Player Luck : " + newPlayer.theLuck);
						Debug.Log ("Player Gold : " + newPlayer.theGold);
				
			
						GameInformation.Invisable = false;
						
						
						Application.LoadLevel ("Town"); 
						
				
				

				}

		}

	}

	private void storeNewPlayerInfo()
	{
		//stores the set values of new player into the gameinfromation script 
		//so it can load the correct values


		GameInformation.PlayerName = newPlayer.thePlayerName;

		GameInformation.PlayerLevel = newPlayer.thePlayerLevel;

		GameInformation.PlayerStamina = newPlayer.theStamina;
		
		GameInformation.PlayerMaxStamina = newPlayer.theMaxStamina;

		GameInformation.PlayerMP = newPlayer.theMP;

		GameInformation.PlayerStrength = newPlayer.theStrength;

		GameInformation.PlayerIntellect = newPlayer.theIntellect;

		GameInformation.PlayerSpeed = newPlayer.theSpeed;

		GameInformation.PlayerLuck = newPlayer.theLuck;
		
	
		

		
		
	}

	


}
