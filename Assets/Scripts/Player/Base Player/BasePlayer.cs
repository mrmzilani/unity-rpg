﻿using UnityEngine;
using System.Collections;

public class BasePlayer  {


	private string playerName;
	private int playerLevel;

	private BaseCharacterClass playerClass;

	private int stamina;
	private int maxStamina;
	private int mp;
	private int strength;
	private int intellect;
	private int speed;
	private int luck;

	private int currentXP;
	private int requiredXP;


	public int theCurrentXP{get;set;}	
	public int theRequiredXP{get;set;}
	
	public int theGold{get;set;}
	

	public string thePlayerName
	{
		get{ return playerName;}
		set{ playerName = value;}
	}


	public int thePlayerLevel
	{
		get{ return playerLevel;}
		set{ playerLevel = value;}
	}


	public BaseCharacterClass thePlayerClass
	{
		get{ return playerClass;}
		set{ playerClass = value;}
	}



	public int theStamina
	{
		get{ return stamina;}
		set{ stamina = value;}
	}
	
	public int theMaxStamina
	{
		get{ return maxStamina;}
		set{ maxStamina = value;}
	}
	
	
	public int theMP
	{
		get{ return mp;}
		set{ mp = value;}
	}
	
	
	public int  theStrength
	{
		get{ return strength;}
		set{ strength = value;}
	}
	
	public int theIntellect
	{
		get{ return intellect;}
		set{ intellect = value;}
	}
	
	
	public int theSpeed
	{
		get{ return speed;}
		set{ speed = value;}
	}
	
	public int theLuck
	{
		get{ return luck;}
		set{ luck = value;}
	}



}
