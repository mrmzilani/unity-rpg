﻿using UnityEngine;
using System.Collections;

public class TestGUI : MonoBehaviour {


	private BaseCharacterClass class1 = new BaseMageClass();
	private BaseCharacterClass class2 = new BaseWarriorClass();

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI()
	{
		GUILayout.Label(class1.theclassName);
		GUILayout.Label(class1.theclassDescription);

		GUILayout.Label(class2.theclassName);
		GUILayout.Label(class2.theclassDescription);

	}
}
