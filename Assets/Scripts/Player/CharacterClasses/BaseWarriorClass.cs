﻿using UnityEngine;
using System.Collections;

public class BaseWarriorClass : BaseCharacterClass {


	public BaseWarriorClass()
	{

		theclassName = "Warrior";
		theclassDescription = "a Warrior class";
		
		thePlayerLevel = 1;
		
		theStrength = 10;
		theMP = 5;
		theIntellect = 1;
		theSpeed = 10;
		theStamina = 50;
		theMaxStamina = 50;
		theLuck = 1;
		theGold = 0;
		

	}



}
