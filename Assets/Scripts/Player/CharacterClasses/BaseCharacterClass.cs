﻿using UnityEngine;
using System.Collections;

public class BaseCharacterClass
{
    private string className;
	private string classDescription;
	private int playerLevel;

	//stats
	private int stamina;
	private int maxStamina;
	private int mp;
	private int strength;
	private int intellect;
	private int speed;
	private int luck;

	private int gold;


	public string theclassName{
		get
		{
			return className;
		}
		set
		{
			className = value;
		}
	}

	public string theclassDescription
	{
		get
		{ 
			return classDescription;
		}
		set
		{ 
			classDescription = value;
		}
	}

	public int thePlayerLevel
	{
		get{ return playerLevel;}
		set{ playerLevel = value;}
	}



	public int theStamina
	{
		get{ return stamina;}
		set{ stamina = value;}
	}

	public int theMaxStamina
	{
		get{ return maxStamina;}
		set{ maxStamina = value;}
	}
	

	public int theMP
	{
		get{ return mp;}
		set{ mp = value;}
	}


	public int  theStrength
	{
		get{ return strength;}
		set{ strength = value;}
	}

	public int theIntellect
	{
		get{ return intellect;}
		set{ intellect = value;}
	}

	
	public int theSpeed
	{
		get{ return speed;}
		set{ speed = value;}
	}

	public int theLuck
	{
		get{ return luck;}
		set{ luck = value;}
	}

	public int theGold
	{
		get{ return gold;}
		set{ gold = value;}
	}

}
