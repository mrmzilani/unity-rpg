﻿using UnityEngine;
using System.Collections;

public class BaseMageClass : BaseCharacterClass {
	
	
	public BaseMageClass()
	{

		theclassName = "Mage";
		theclassDescription = "a Mage which attacks with spells";
		thePlayerLevel = 1;

		theStrength = 1;
		theMP = 15;
		theIntellect = 10;
		theSpeed = 3;
		theStamina = 30;
		theMaxStamina = 30;
		theLuck = 1;
		theGold = 0;
	}



}
