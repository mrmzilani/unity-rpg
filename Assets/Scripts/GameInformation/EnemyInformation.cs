﻿using UnityEngine;
using System.Collections;

public class EnemyInformation : MonoBehaviour {


	public static string EnemyName{get;set;}
	public static int EnemyLevel{get;set;}
	public static int EnemyStamina{get;set;}
	public static int EnemyMP{get;set;}
	public static int EnemyStrength{get;set;}
	public static int EnemyIntellect{get;set;}
	public static int EnemySpeed{get;set;}
	public static int EnemyLuck{get;set;}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
}
