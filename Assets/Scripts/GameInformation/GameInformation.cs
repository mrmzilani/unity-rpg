﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameInformation : MonoBehaviour {


	private static bool created = false;

	void Awake () {

		if (!created) 
		{
			DontDestroyOnLoad (transform.gameObject);
			created = true;
			
		} 
		else 
		{
			Destroy(this.gameObject); 
		}
		

	}


	//public static int PlayerGold = 0;

	public static string PlayerName{get;set;}
	public static int PlayerLevel{get;set;}
	public static BaseCharacterClass PlayerClass{get;set;}
	public static int PlayerStamina{get;set;}
	public static int PlayerMaxStamina{get;set;}
	public static int PlayerMP{get;set;}
	public static int PlayerStrength{get;set;}
	public static int PlayerIntellect{get;set;}
	public static int PlayerSpeed{get;set;}
	public static int PlayerLuck{get;set;}
	


	public static int theCurrentXP{get;set;}	
	public static int theRequiredXP{get;set;}
	public static int theGold{get;set;}

	public static BaseEquipment Accessory{get;set;}
	public static BasePotion Potion{get;set;}

	public static bool Invisable{get;set;}
	//Used to check if the player is in a battle
	
	public static bool IsNewPlayer{get;set;}


	//List of Items in the Players inventory

	public static List<BasePotion> ListofPotion = new List<BasePotion>();
	public static int PotionSlot0{get;set;}
	public static int PotionSlot1{get;set;}
	public static int PotionSlot2{get;set;}
	public static int PotionSlot3{get;set;}
	public static int PotionSlot4{get;set;}
	public static int PotionSlot5{get;set;}
	public static int PotionSlot6{get;set;}
	public static int PotionSlot7{get;set;}
	public static int PotionSlot8{get;set;}
	public static int PotionSlot9{get;set;}
	public static bool LoadPotionsOnce{get;set;}
	
	
	// Use this for initialization
	void Start () {
		
		LoadPotionsOnce = true;
	
	}
	
	// Update is called once per frame
	void Update () {
	
		//Sets the amount for the next level 
		GameInformation.theRequiredXP = GameInformation.PlayerLevel * 100;
		
	
	}
}
