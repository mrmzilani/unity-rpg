﻿using UnityEngine;
using System.Collections;

public class ParticleEffects : MonoBehaviour {

	//Reference: http://pixelnest.io/tutorials/2d-game-unity/particles/

	/// <summary>
	/// Singleton
	/// </summary>
	public static ParticleEffects Instance;
	
	public ParticleSystem smokeEffect;
	public ParticleSystem healEffect;
	public ParticleSystem fireballEffect;
	
	void Awake()
	{
		// Register the singleton
		if (Instance != null)
		{
			Debug.LogError("Multiple instances of SpecialEffectsHelper!");
		}
		
		Instance = this;
		
	}
	
	/// <summary>
	/// Create an explosion at the given location
	/// </summary>
	/// <param name="position"></param>
	public void Explosion(Vector3 position)
	{
		instantiate(smokeEffect, position);	
	}
	
	public void Healing (Vector3 position)
	{

		instantiate(healEffect, position);

	}
	
	
	public void Fireball (Vector3 position)
	{
		
		instantiate(fireballEffect, position);
		
	}
	
	
	
	/// <summary>
	/// Instantiate a Particle system from prefab
	/// </summary>
	/// <param name="prefab"></param>
	/// <returns></returns>
	private ParticleSystem instantiate(ParticleSystem prefab, Vector3 position)
	{
		ParticleSystem newParticleSystem = Instantiate(
			prefab,
			position,
			Quaternion.identity
			) as ParticleSystem;
		
		// Make sure it will be destroyed
		Destroy(
			newParticleSystem.gameObject,
			newParticleSystem.startLifetime
			);
		
		return newParticleSystem;
	}
	
	
}
