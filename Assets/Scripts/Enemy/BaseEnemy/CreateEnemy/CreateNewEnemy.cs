﻿using UnityEngine;
using System.Collections;

public class CreateNewEnemy : MonoBehaviour {


	public BaseEnemy randomMonster;

	// Use this for initialization
	void Start () {


	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void CreateMonsterEnemy()
	{
	
		randomMonster = new BaseEnemy ();

		randomMonster.theEnemyClass = new EnemyMonster();
		 
		//store enemy info so other classes can acess it
		EnemyInformation.EnemyStamina = randomMonster.theEnemyClass.theStamina;
		EnemyInformation.EnemyMP = randomMonster.theEnemyClass.theMP;
		EnemyInformation.EnemyName = randomMonster.theEnemyClass.theEnemyName;
		EnemyInformation.EnemyStrength = randomMonster.theEnemyClass.theStrength;

		Debug.Log ("Name: " + randomMonster.theEnemyName);
		Debug.Log ("Level: " + randomMonster.theEnemyLevel);
		Debug.Log ("Stamina: " + randomMonster.theStamina);
		Debug.Log ("MP : " + randomMonster.theMP);

		Debug.Log ("ENEMY CREATED");

	}
	
	public void CreateBearEnemy()
	{
		randomMonster = new BaseEnemy ();
		
		randomMonster.theEnemyClass = new EnemyBear();
		
		//store enemy info so other classes can acess it
		EnemyInformation.EnemyStamina = randomMonster.theEnemyClass.theStamina;
		EnemyInformation.EnemyMP = randomMonster.theEnemyClass.theMP;
		EnemyInformation.EnemyName = randomMonster.theEnemyClass.theEnemyName;
		EnemyInformation.EnemyStrength = randomMonster.theEnemyClass.theStrength;
		
		Debug.Log ("Name: " + randomMonster.theEnemyName);
		Debug.Log ("Level: " + randomMonster.theEnemyLevel);
		Debug.Log ("Stamina: " + randomMonster.theStamina);
		Debug.Log ("MP : " + randomMonster.theMP);
		
		Debug.Log ("ENEMY CREATED");
		
	}
	
	
	public void CreateEyeEnemy()
	{
		
		randomMonster = new BaseEnemy();
		
		randomMonster.theEnemyClass = new EnemyEye();
		
		//store enemy info so other classes can acess it
		EnemyInformation.EnemyStamina = randomMonster.theEnemyClass.theStamina;
		EnemyInformation.EnemyMP = randomMonster.theEnemyClass.theMP;
		EnemyInformation.EnemyName = randomMonster.theEnemyClass.theEnemyName;
		EnemyInformation.EnemyStrength = randomMonster.theEnemyClass.theStrength;
		
		Debug.Log ("Name: " + randomMonster.theEnemyName);
		Debug.Log ("Level: " + randomMonster.theEnemyLevel);
		Debug.Log ("Stamina: " + randomMonster.theStamina);
		Debug.Log ("MP : " + randomMonster.theMP);
		
		Debug.Log ("ENEMY CREATED");
		
	}
	

}
