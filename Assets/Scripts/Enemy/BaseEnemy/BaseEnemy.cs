﻿using UnityEngine;
using System.Collections;

public class BaseEnemy  {
	// used to instanciate the various enemies in the game


	private string EnemyName;
	private int EnemyLevel;

	private BaseEnemyClass EnemyClass;

	//stats
	private int stamina;
	private int mp;
	private int strength;
	private int intellect;
	private int speed;
	private int luck;

	
	public BaseEnemyClass theEnemyClass
	{
		get{ return EnemyClass;}
		set{ EnemyClass = value;}
	}



	public string theEnemyName{
		get
		{
			return EnemyName;
		}
		set
		{
			EnemyName = value;
		}
	}
	
	public int theEnemyLevel
	{
		get{ return EnemyLevel;}
		set{ EnemyLevel = value;}
	}
	
	
	
	public int theStamina
	{
		get{ return stamina;}
		set{ stamina = value;}
	}
	
	public int theMP
	{
		get{ return mp;}
		set{ mp = value;}
	}
	
	
	public int  theStrength
	{
		get{ return strength;}
		set{ strength = value;}
	}
	
	public int theIntellect
	{
		get{ return intellect;}
		set{ intellect = value;}
	}
	
	
	public int theSpeed
	{
		get{ return speed;}
		set{ speed = value;}
	}
	
	public int theLuck
	{
		get{ return luck;}
		set{ luck = value;}
	}






}
