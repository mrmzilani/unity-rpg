﻿using UnityEngine;
using System.Collections;

public class BaseEnemyClass {
	// A general class for all the information inside an enemy

	private string enemyName;

	private int enemyLevel;
	
	//stats
	private int stamina;
	private int mp;
	private int strength;
	private int intellect;
	private int speed;
	private int luck;
	
	private int enemyGold;
	
	public string theEnemyName{
		get
		{
			return enemyName;
		}
		set
		{
			enemyName = value;
		}
	}

	
	public int theEnemyLevel
	{
		get{ return enemyLevel;}
		set{ enemyLevel = value;}
	}
	
	
	
	public int theStamina
	{
		get{ return stamina;}
		set{ stamina = value;}
	}
	
	public int theMP
	{
		get{ return mp;}
		set{ mp = value;}
	}
	
	
	public int  theStrength
	{
		get{ return strength;}
		set{ strength = value;}
	}
	
	public int theIntellect
	{
		get{ return intellect;}
		set{ intellect = value;}
	}
	
	
	public int theSpeed
	{
		get{ return speed;}
		set{ speed = value;}
	}
	
	public int theLuck
	{
		get{ return luck;}
		set{ luck = value;}
	}


	

}
