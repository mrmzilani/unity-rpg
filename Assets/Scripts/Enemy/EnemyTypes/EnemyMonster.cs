﻿using UnityEngine;
using System.Collections;

public class EnemyMonster : BaseEnemyClass {
	//Specific instances of the base enemy class

	public EnemyMonster()
	{

		theEnemyName = "Monster";

		theEnemyLevel = 1;

		theStrength = 4;
		theMP = 5;
		theIntellect = 1;
		theSpeed = 10;
		theStamina = 15;
		theLuck = 1;
		


	}



}
