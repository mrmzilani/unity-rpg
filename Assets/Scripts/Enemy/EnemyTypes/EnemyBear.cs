﻿using UnityEngine;
using System.Collections;

public class EnemyBear : BaseEnemyClass {

	public EnemyBear()
	{
		
		theEnemyName = "Angry Bear";
		
		theEnemyLevel = 1;
		
		theStrength = 4;
		theMP = 5;
		theIntellect = 1;
		theSpeed = 3;
		theStamina = 25;
		theLuck = 1;
		
		
		
	}
}
