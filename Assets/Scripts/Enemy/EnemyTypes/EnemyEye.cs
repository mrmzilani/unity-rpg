﻿using UnityEngine;
using System.Collections;

public class EnemyEye : BaseEnemyClass {

	public EnemyEye()
	{
		
		theEnemyName = "Evil Eye";
		
		theEnemyLevel = 1;
		
		theStrength = 1;
		theMP = 5;
		theIntellect = 5;
		theSpeed = 3;
		theStamina = 8;
		theLuck = 1;
		
		
		
	}
}
