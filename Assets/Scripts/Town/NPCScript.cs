﻿using UnityEngine;
using System.Collections;

public class NPCScript : MonoBehaviour {



	public GameObject NPC;
	
	
	public bool talking;
	public string Say;
	public string Name;
	
	public int boxLength = 100;
	public int boxWidth = 32;
	public int boxHeight= 300;
	
	// Use this for initialization
	void Start () {
	
		talking = false;
		
		Name = Name + ": ";
	
	
	}
	
	// Update is called once per frame
	void Update () {
	
		Debug.Log(talking);
		

				
		
	}
	
	void OnGUI()
	{
	
		if(talking)
		{
			GUI.Button(new Rect(((Screen.width-boxLength)/2),boxHeight,boxLength,boxWidth),Name + Say);
		}
		
	}
	
	void OnCollisionEnter2D(Collision2D col) //when colliding talk
	{
		if(col.gameObject.tag == "Player")
		{	
			Debug.Log("Talk");
			talking = true;

		}
					
	}
	
	
	
	void OnTriggerExit2D(Collider2D col)//no collision talk false
	{
		talking = false;
	}


	


	

	
}
