﻿using UnityEngine;
using System.Collections;

public class MainMenuScript : MonoBehaviour {


	// Use this for initialization
	void Start () {
	
		GameInformation.IsNewPlayer = false;
	
	
	}
	
	// Update is called once per frame
	void Update () {
	
	
	}
	

	
	void OnGUI()
	{
	
		if (GUI.Button (new Rect (0, 0, 80, 32), "New Game")) 
		{
		

			Application.LoadLevel ("CreateCharacter"); 
			
			
		}
	
		if (GUI.Button (new Rect (0, 64, 64, 32), "Load")) 
		{

			LoadInformation.LoadAllInfomation();
			Application.LoadLevel("Town"); 
			GameInformation.Invisable = false;
			
		}
		
		
		if (GUI.Button (new Rect (0, 128, 64, 32), "Quit")) 
		{
			Application.Quit();
			
		}
		
	
	}
	
}
