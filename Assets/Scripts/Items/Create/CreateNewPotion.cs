﻿using UnityEngine;
using System.Collections;

public class CreateNewPotion : MonoBehaviour {


	private BasePotion newPotion;

	void Start () {
	
//		CreatePotion ();

		Debug.Log (newPotion.theItemName);
		Debug.Log (newPotion.theItemID);
		Debug.Log (newPotion.theItemDescription);
		Debug.Log (newPotion.thePotionType);

	}
	
	public void CreatePotion(BasePotion target) //creates a potion at the target
	{
	
		//sets up a new potion
		newPotion = new BasePotion (); 

		//newPotion.theItemDescription = "This is a potion";
		ChoosePotionType (Random.Range (1, 7));
		
		//sends potion infomation to the target
		target.theItemName = newPotion.theItemName;
		target.theItemDescription = newPotion.theItemDescription;
		target.theItemID = newPotion.theItemID;
		target.thePotionType = newPotion.thePotionType;
		target.ThePotionValue = newPotion.ThePotionValue;
		
		//LOGpotion();
	}
	
	public void CreateSpecificPotion(BasePotion target,int type) //creates a potion at the target
	{
		
		//sets up a new potion
		newPotion = new BasePotion (); 
		
		//newPotion.theItemDescription = "This is a potion";
		
		ChoosePotionType(type);
		
		//sends potion infomation to the target
		target.theItemName = newPotion.theItemName;
		target.theItemDescription = newPotion.theItemDescription;
		target.theItemID = newPotion.theItemID;
		target.thePotionType = newPotion.thePotionType;
		target.ThePotionValue = newPotion.ThePotionValue;
		
		//LOGpotion();
	}


	private void ChoosePotionType(int value)
	{

		switch (value) 
		{
			
		case 1:
			newPotion.thePotionType = BasePotion.PotionTypes.HEALTH;
			newPotion.theItemName = "HP Potion";
			newPotion.theItemDescription = "This potion slightly increases HP";
			newPotion.ThePotionValue = 10;
			newPotion.theItemID = 1;
			break;
			
		case 2:
			newPotion.thePotionType = BasePotion.PotionTypes.MP;
			newPotion.theItemName = "MP Potion";
			newPotion.theItemDescription = "This potion slightly increases MP";
			newPotion.ThePotionValue = 10;
			newPotion.theItemID = 2;
			break;
			
		case 3:
			newPotion.thePotionType = BasePotion.PotionTypes.INTELLECT;
			newPotion.theItemName = "Intellect Potion";
			newPotion.theItemDescription = "This potion slightly increases the Intellect stat with a random value";
			newPotion.ThePotionValue = Random.Range (1, 3);
			newPotion.theItemID = 3;
			break;
			
		case 4:
			newPotion.thePotionType = BasePotion.PotionTypes.LUCK;
			newPotion.theItemName = "Chance Potion";
			newPotion.theItemDescription = "This potion slightly increases the Intellect stat with a random value ";
			newPotion.ThePotionValue = Random.Range (1, 3);;
			newPotion.theItemID = 4;
			break;
			
		case 5:
			newPotion.thePotionType = BasePotion.PotionTypes.SPEED;
			newPotion.theItemName = "Quick Potion";
			newPotion.theItemDescription = "This potion slightly increases the Speed stat with a random value ";
			newPotion.ThePotionValue = Random.Range (1, 3);
			newPotion.theItemID = 5;
			break;
			
		case 6:
			newPotion.thePotionType = BasePotion.PotionTypes.STRENGTH;
			newPotion.theItemName = "strength Potion";
			newPotion.theItemDescription = "This potion slightly increases the Strength stat with a random value ";
			newPotion.ThePotionValue = Random.Range (1, 3);
			newPotion.theItemID = 6;
			break;
			
		case 7:
		
			newPotion.theItemName = "EMPTY";
			newPotion.theItemID = 7;
			break;
			
			
			
		}

	}
	
	void LOGpotion()
	{
		Debug.Log (newPotion.theItemName);
		Debug.Log (newPotion.theItemID);
		Debug.Log (newPotion.theItemDescription);
		Debug.Log (newPotion.thePotionType);
	}


}
