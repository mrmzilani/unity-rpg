﻿using UnityEngine;
using System.Collections;

public class CreateNewWeapon : MonoBehaviour {


	void Start()
	{

		CreateWeapon ();

		Debug.Log (newWeapon.theItemName);
		Debug.Log (newWeapon.theItemID);
		Debug.Log (newWeapon.theItemDescription);
		Debug.Log (newWeapon.theWeaponType);
		Debug.Log (newWeapon.theSpeed);
		Debug.Log (newWeapon.theStrength);

	}


	private BaseWeapon newWeapon;




	public void CreateWeapon()
	{

		newWeapon = new BaseWeapon ();

		newWeapon.theItemName= "W" + Random.Range (1, 101); //random names
		//change this later


		newWeapon.theItemDescription = "this is a new weapon";

		newWeapon.theItemID = Random.Range (1, 101);

		newWeapon.theStamina = Random.Range (1, 10);
		newWeapon.theIntellect = Random.Range (1, 10);
		newWeapon.theMP = Random.Range (1, 10);
		newWeapon.theLuck= Random.Range (1, 10);
		newWeapon.theSpeed = Random.Range (1, 10);
		newWeapon.theSpellEffectID = Random.Range (1, 100);

		ChooseWeaponType ();

	}

	private void ChooseWeaponType()
	{

		int randomTemp = Random.Range (1, 6);


		switch (randomTemp) 
		{

		case 1:
			newWeapon.theWeaponType = BaseWeapon.WeaponTypes.SWORD;
			break;

		case 2:
			newWeapon.theWeaponType = BaseWeapon.WeaponTypes.AXE;
			break;

		case 3:
			newWeapon.theWeaponType = BaseWeapon.WeaponTypes.BOW;
			break;

		case 4:
			newWeapon.theWeaponType = BaseWeapon.WeaponTypes.DAGGER;
			break;

		case 5:
			newWeapon.theWeaponType = BaseWeapon.WeaponTypes.STAFF;
			break;

		case 6:
			break;

		case 7:
			newWeapon.theWeaponType = BaseWeapon.WeaponTypes.SHIELD;
			break;



		}



	}

}
