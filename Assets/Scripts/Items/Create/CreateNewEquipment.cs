﻿using UnityEngine;
using System.Collections;

public class CreateNewEquipment : MonoBehaviour {



	void Start()
	{
	
		CreateEquipment();
		Debug.Log (newEquipment.theItemName);
		Debug.Log (newEquipment.theItemID);
		Debug.Log (newEquipment.theItemDescription);
		Debug.Log (newEquipment.theEquipmentType);
		Debug.Log (newEquipment.theSpeed);


	}

	private BaseEquipment newEquipment;
	private string[] itemNames = new string[4]{"Common","Rare","Super","Legendary"};
	private string[] itemDes = new string[2]{"normal item","Good item"};


	public void CreateEquipment()
	{

		newEquipment = new BaseEquipment();
		newEquipment.theItemName = itemNames [Random.Range(0,itemDes.Length)];
		newEquipment.theItemID = Random.Range(1,101);


		newEquipment.theItemDescription = itemDes [Random.Range (1, 2)];
		

		newEquipment.theStamina = Random.Range (1, 10);
		newEquipment.theIntellect = Random.Range (1, 10);
		newEquipment.theMP = Random.Range (1, 10);
		newEquipment.theLuck= Random.Range (1, 10);
		newEquipment.theSpeed = Random.Range (1, 10);
		newEquipment.theSpellEffectID = Random.Range (1, 1);



		ChooseEquipmentType();
	

	}


	public void ChooseEquipmentType()
	{
				int randomTemp = Random.Range (1, 5);
		
		
				switch (randomTemp) 
				{
			
				case 1:
						newEquipment.theEquipmentType = BaseEquipment.EquipmentTypes.HEAD;
						break;
			
				case 2:
						newEquipment.theEquipmentType = BaseEquipment.EquipmentTypes.CHEST;
						break;
			
				case 3:
						newEquipment.theEquipmentType = BaseEquipment.EquipmentTypes.ARMS;
						break;
			
				case 4:
						newEquipment.theEquipmentType = BaseEquipment.EquipmentTypes.LEGS;
						break;
				}
	}

}
