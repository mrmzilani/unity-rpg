﻿using UnityEngine;
using System.Collections;

[System.Serializable]

public class BasePotion : BaseItem{


	public enum PotionTypes
	{
		HEALTH,
		STRENGTH,
		MP,
		INTELLECT,
		SPEED,
		LUCK
		
	}

	private PotionTypes potionType;
	private int potionValue;
	
	public PotionTypes thePotionType
	{
		get{ return potionType;}
		set{potionType = value;}
		
	}
	
	public int ThePotionValue
	{
		get{ return potionValue;}
		set{ potionValue = value;}
	}
	


}
