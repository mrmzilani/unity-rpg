﻿using UnityEngine;
using System.Collections;

[System.Serializable]

public class BaseEquipment : BaseItem {


	public enum EquipmentTypes{
	
			HEAD,
			CHEST,
			ARMS,
			LEGS,
			ACCESSORY,

	}



	////////////////////////////////////////////////
	
	private EquipmentTypes equipmentType;
	
	public EquipmentTypes theEquipmentType
	{
		get{ return equipmentType;}
		set{equipmentType = value;}
		
	}
	



}
