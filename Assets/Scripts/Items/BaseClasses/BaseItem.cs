﻿using UnityEngine;
using System.Collections;


[System.Serializable]

public class BaseItem  {

	private string itemName;
	private string itemDescription;
	private int itemID;

	private int stamina;
	private int mp;
	private int strength;
	private int intellect;
	private int speed;
	private int luck;
	
	public enum ItemTypes{

		EQUIPMENT,
		WEAPON,
		POTION,

	}

	private ItemTypes itemType;


	public string theItemName 
	{
		get{ return itemName;}
		set{ itemName = value;}
	}

	public string theItemDescription 
	{
		get{ return itemDescription;}
		set{ itemDescription = value;}
	}

	public int theItemID
	{
		get{ return itemID;}
		set{ itemID = value;}
	}


	public ItemTypes theItemTypes
	{
		get{ return itemType;}
		set{ itemType = value;}
	}


	private int spellEffectID;
	
	public int theSpellEffectID
	{
		get{ return spellEffectID;}
		set{ spellEffectID = value;}
	}

	public int theStamina
	{
		get{ return stamina;}
		set{ stamina = value;}
	}
	
	public int theMP
	{
		get{ return mp;}
		set{ mp = value;}
	}
	
	
	public int  theStrength
	{
		get{ return strength;}
		set{ strength = value;}
	}
	
	public int theIntellect
	{
		get{ return intellect;}
		set{ intellect = value;}
	}
	
	
	public int theSpeed
	{
		get{ return speed;}
		set{ speed = value;}
	}
	
	public int theLuck
	{
		get{ return luck;}
		set{ luck = value;}
	}



}
