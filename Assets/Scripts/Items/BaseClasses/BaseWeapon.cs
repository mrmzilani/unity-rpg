﻿using UnityEngine;
using System.Collections;

[System.Serializable]

public class BaseWeapon : BaseItem{
	
	public enum WeaponTypes
	{
		SWORD,
		STAFF,
		DAGGER,
		BOW,
		SHIELD,
		AXE

	}
	
	private WeaponTypes weaponType;

	public WeaponTypes theWeaponType
	{
		get{ return weaponType;}
		set{weaponType = value;}
	
	}





}
