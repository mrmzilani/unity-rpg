﻿using UnityEngine;
using System.Collections;

public class SpawnManager : MonoBehaviour {


	public GameObject enemy;

	public float currentTime;

	public int spawnTime = 5;
	private int resetSpawnTimer;

	// Use this for initialization
	void Start () {

		resetSpawnTimer = spawnTime + 1;
		
	}
	
	// Update is called once per frame
	void Update () {
	


		currentTime += Time.deltaTime;

		if (currentTime >= resetSpawnTimer) 
		{
			currentTime = 0;
		}
	
		if (currentTime >= spawnTime) 
		{	
			Instantiate(enemy);
			currentTime = 0;
		
		}



	}
}
