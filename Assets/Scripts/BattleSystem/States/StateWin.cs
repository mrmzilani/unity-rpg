﻿using UnityEngine;
using System.Collections;

public class StateWin : MonoBehaviour {

	
	bool xpAdded = false;
	

	// Use this for initialization
	void Start () {
	
		xpAdded = true;
		
	}
	
	// Update is called once per frame
	void Update () {

		if(xpAdded == true)
		{
			IncreaseExperience.AddExperience();
			EndBattleRewards.IncreaseGold();
			
			xpAdded = false;
		}
		

		
		

		
	
	}
	
	void OnGUI()
	{
			
		if( TurnBasedCombatStateMachine.currentState == TurnBasedCombatStateMachine.BattleStates.WIN)
		{
			if(GUILayout.Button("Back To World"))
			{
				GameInformation.Invisable = false; //Battle is ended player exited battle
				SaveInformation.SaveAllInfomation();
				Application.LoadLevel("WorldMapScene"); 
			}
		}
	
	}
	
}
