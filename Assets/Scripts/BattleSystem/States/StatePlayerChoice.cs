﻿using UnityEngine;
using System.Collections;

public class StatePlayerChoice : MonoBehaviour {
	
	
	public static bool Attack;
	public static bool Defend;
	public static bool Run;
	
	public float wait;
	

	public bool selectedMagic;
	
	public AudioClip Fire;
	public AudioClip Heal;
	
	public AudioClip selectSound;
	
	public static bool EnemyDead;
	

	Animator A;
	
	// Use this for initialization
	void Start () {
		
		Defend = false;
		Attack = false;
		Run = false;
		EnemyDead = false;
		
		selectedMagic = false;
		
		A = GetComponent<Animator>();
		

		
	}
	
	// Update is called once per frame
	void Update () {
		
		
	
		

	}
	
	
	public void OnGUI()
	{
		
		if(TurnBasedCombatStateMachine.currentState == TurnBasedCombatStateMachine.BattleStates.PLAYERCHOICE)
		{
			if (GUI.Button (new Rect (Screen.width - 64, 0, 64, 32), "Attack")) 
			{
				
				audio.clip = selectSound;
				audio.Play ();
				
				
				
				Defend = false;
				Attack = true;
				
				
				A.SetTrigger("DidPlayerAttack");
				
				EnemyInformation.EnemyStamina = CalculateDamage.Attack(GameInformation.PlayerStrength,EnemyInformation.EnemyStamina);
				
				TurnBasedCombatStateMachine.currentState = TurnBasedCombatStateMachine.BattleStates.ENEMYCHOICE;
			
				if(EnemyInformation.EnemyStamina <= 0)
				{
					EnemyInformation.EnemyStamina = 0;
					EnemyDead = true;
					
					TurnBasedCombatStateMachine.currentState = TurnBasedCombatStateMachine.BattleStates.WIN;
					
				}

			}
			
			if (GUI.Button (new Rect (Screen.width - 64, 33, 64, 32), "Magic")) 
			{
				
				audio.clip = selectSound;
				audio.Play ();
				
				Defend = false;
				Attack = true;
				
				if(selectedMagic == false)
				{
					selectedMagic = true;
				}
				
	
			}
			
			
			
			
				if(selectedMagic == true)
				{
				
					if (GUI.Button (new Rect (Screen.width - 128, 33, 64, 32), "Fire")) 
					{
				
						selectedMagic = false;
						
						audio.clip = Fire;
						audio.Play ();
						
						ParticleEffects.Instance.Fireball(new Vector3(transform.position.x-8,transform.position.y,transform.position.z));
					
						EnemyInformation.EnemyStamina = CalculateDamage.UseAttackSpell(GameInformation.PlayerIntellect,EnemyInformation.EnemyStamina);
						
						
						selectedMagic = false;
						TurnBasedCombatStateMachine.currentState = TurnBasedCombatStateMachine.BattleStates.ENEMYCHOICE;
					
					
						if(EnemyInformation.EnemyStamina <= 0)
						{
							EnemyInformation.EnemyStamina = 0;
							EnemyDead = true;
							TurnBasedCombatStateMachine.currentState = TurnBasedCombatStateMachine.BattleStates.WIN;

						}
				
					}
				
					if (GUI.Button (new Rect (Screen.width - 128, 66, 64, 32), "Heal")) 
					{	
						selectedMagic = false;
						
					
						audio.clip = Heal;
						audio.Play ();
					
						
						
						ParticleEffects.Instance.Healing(transform.position);
						GameInformation.PlayerStamina += 6;
	
						TurnBasedCombatStateMachine.currentState = TurnBasedCombatStateMachine.BattleStates.ENEMYCHOICE;
				
				
					}
					
					if (GUI.Button (new Rect (Screen.width - 128, 99, 64, 32), "Close")) 
					{	
						audio.clip = selectSound;
						audio.Play ();
						selectedMagic = false;		
					}
				
				}
	
			
			
			
			
			
			if (GUI.Button (new Rect (Screen.width - 64, 66, 64, 32), "Defend")) 
			{
				
				
				Defend = true;
				Attack = false;
				
				audio.clip = selectSound;
				audio.Play ();
				
				A.SetTrigger("DidPlayerDefend");
				
				TurnBasedCombatStateMachine.currentState = TurnBasedCombatStateMachine.BattleStates.ENEMYCHOICE;
				
				
			}
			
		/*	if (GUI.Button (new Rect (Screen.width - 64,99, 64, 32), "Item")) 
			{
				
				Defend = true;
				Attack = false;
				
				GameInformation.PlayerStamina += 5;
				TurnBasedCombatStateMachine.currentState = TurnBasedCombatStateMachine.BattleStates.ENEMYCHOICE;
				
			}*/
			
			if (GUI.Button (new Rect (Screen.width - 64,99, 64, 32), "Run")) 
			{
				
				audio.clip = selectSound;
				audio.Play ();
				
				int chance = 0; //Chance of running away from a battle
				
				chance = Random.Range(1,100);
				
				if(chance <= 80)
				{
					A.SetTrigger("DidPlayerRun");
					GameInformation.Invisable = false; //Battle is ended
					Application.LoadLevel("WorldMapScene"); 
				}
				else
				{
					A.SetTrigger("DidPlayerRun");
					Run = true;
					TurnBasedCombatStateMachine.currentState = TurnBasedCombatStateMachine.BattleStates.ENEMYCHOICE;
				}
				
				
			}
			
		}
		
		
		
	}
}
