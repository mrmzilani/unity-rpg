﻿using UnityEngine;
using System.Collections;

public class StateStart : MonoBehaviour{

	//public static BaseEnemy RandomSoldier;

	public static bool TypeMonster;
	public static bool TypeBear;
	public static bool TypeEye;

	int chooseEnemy;
	
	
	public AudioClip monsterSound; 
	public AudioClip bearSound; 
	public AudioClip eyeSound; 

	void Awake () 
	{
		
	}

	CreateNewEnemy create  = new CreateNewEnemy();

	// Use this for initialization
	void Start () {

		TypeMonster = false;
		TypeBear = false;
		

		chooseEnemy = 0;

		chooseEnemy = Random.Range(1,4);
		
		Debug.Log("NUMBER IS" + chooseEnemy);
		
		switch(chooseEnemy)
		{
		
			case 1:
			
			create.CreateMonsterEnemy ();
			TypeMonster = true;
			
			audio.clip = monsterSound;
			audio.Play();
			
			
			break;
			
			
			case 2:	
			
			create.CreateBearEnemy();
			TypeBear = true;
			
			audio.clip = bearSound;
			audio.Play();
			
			break;
			
			
			
			case 3:
			
			create.CreateEyeEnemy();	
			TypeEye = true;
			
			audio.clip = eyeSound;
			audio.Play();
		
			break;
			
			
			
			
			default:
			break;
		
		}

		//create.CreateMonsterEnemy ();		
		// as soon as battle starts it creates an enemy
		

		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnGUI()
	{
	
	
	}
}
