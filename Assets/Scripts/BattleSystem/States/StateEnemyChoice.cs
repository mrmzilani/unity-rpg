﻿using UnityEngine;
using System.Collections;

public class StateEnemyChoice : MonoBehaviour {



	private bool attackPlayer;
	public float enemyThinkTime;
	public static bool enemyattacked;
	// Use this for initialization

	public static bool playerDead;

	void Start () {

		playerDead = false;
	}
	
	// Update is called once per frame
	void Update () {
	

		if(GameInformation.PlayerStamina <= 0)
		{
			GameInformation.PlayerStamina = 0;
			playerDead = true;;
			
			TurnBasedCombatStateMachine.currentState = TurnBasedCombatStateMachine.BattleStates.LOSE;
		
		}
		
		
		if(TurnBasedCombatStateMachine.currentState == TurnBasedCombatStateMachine.BattleStates.ENEMYCHOICE)
		{
			enemyThinkTime+= Time.deltaTime;
			
			
			if(enemyThinkTime >= 2.8 )
			{
				enemyattacked = true;
				
			}
			

			if(enemyThinkTime >= 3)
			{
				enemyattacked = false;
				enemyThinkTime = 0;
				Choose();
			}
			
	

			
			
		}

	}
	
	
	public void Choose()
	{

		


		if(StatePlayerChoice.Attack == true || StatePlayerChoice.Run == true)
		{
		
		
		
		GameInformation.PlayerStamina = CalculateDamage.Attack(EnemyInformation.EnemyStrength,GameInformation.PlayerStamina);
			
			
		TurnBasedCombatStateMachine.currentState = TurnBasedCombatStateMachine.BattleStates.PLAYERCHOICE;
		
		}
		
		if(StatePlayerChoice.Defend == true)
		{
		
		
			GameInformation.PlayerStamina = CalculateDamage.Defend(EnemyInformation.EnemyStrength,GameInformation.PlayerStamina,GameInformation.PlayerStrength);
			TurnBasedCombatStateMachine.currentState = TurnBasedCombatStateMachine.BattleStates.PLAYERCHOICE;
		}
		
		
	}
	
	
	
	
	
}
