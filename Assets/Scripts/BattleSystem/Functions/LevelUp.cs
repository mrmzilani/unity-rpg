﻿using UnityEngine;
using System.Collections;

public class LevelUp {

	public int maxPlayerLevel = 50;
	
	public void LevelUpCharacter()
	{
	
		//check if current xp > required xp
	
		if(GameInformation.theCurrentXP > GameInformation.theRequiredXP)
		{
			GameInformation.theCurrentXP -= GameInformation.theRequiredXP;
			//keeps carried over XP
		}
		else
		{
			GameInformation.theCurrentXP = 0;
		}
	
		
		if(GameInformation.PlayerLevel <= maxPlayerLevel) 
		{
			
			GameInformation.PlayerLevel += 1;
			
			GameInformation.PlayerMaxStamina += Random.Range(1,2);
			GameInformation.PlayerSpeed += Random.Range(1,2);
			GameInformation.PlayerStrength += Random.Range(1,2);
			GameInformation.PlayerIntellect += Random.Range(1,2);
			GameInformation.PlayerLuck += Random.Range(1,2);
			
			
			Debug.Log ("Player Leveled Up!!");
		}
		else
		{
			GameInformation.PlayerLevel = maxPlayerLevel;
		}
		
		
		
	
	}
	
	
	public void DetermineRequiredXP()
	{
		Debug.Log ("New required XP");
		
		int temp = (GameInformation.PlayerLevel * 100);
	
		GameInformation.theRequiredXP = temp;
	
	}
	

	
}
