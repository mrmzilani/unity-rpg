﻿using UnityEngine;
using System.Collections;

public static class IncreaseExperience{
	
	private static int xpToGive;
	private static LevelUp levelupscript = new LevelUp();
	
	
	
	public static void AddExperience()
	{	
		
		GameInformation.theCurrentXP += 100;
		
		CheckIfLeveled(); 
		
		Debug.Log(xpToGive);
	}
	
	
	
	public static void CheckIfLeveled()
	{
		if(GameInformation.theCurrentXP >= GameInformation.theRequiredXP)
		{
			//Player leveled up
			levelupscript.LevelUpCharacter();
			
		}
	}
	
	
	
	

}
