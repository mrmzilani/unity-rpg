﻿using UnityEngine;
using System.Collections;

public class EnemyBattleSprite : MonoBehaviour {


	public Sprite EnemyMonsterSprite;
	public Sprite EnemyBearSprite;
	public Sprite EnemyEyeSprite;
	float timer;
	Animator A;
	
	

	// Use this for initialization
	void Start () {
		
		A = GetComponent<Animator>();
		
		A.SetBool("IsMonster",false);
		A.SetBool("IsBear",false);
	
	}
	
	// Update is called once per frame
	void Update () {
	
		SpriteRenderer SR = GetComponent<SpriteRenderer>();
		
	/*	if (SR.sprite == null) // if the sprite on spriteRenderer is null then
		{
			SR.sprite = EnemyMonsterSprite; // set the sprite to sprite1
		}*/
		
		if(StateStart.TypeMonster == true)
		{
			A.SetBool("IsMonster",true);
			SR.sprite = EnemyMonsterSprite;
		
		}			
		
		if(StateStart.TypeBear == true)
		{
			A.SetBool("IsBear",true);
			SR.sprite = EnemyBearSprite;
		}		
		
		if(StateStart.TypeEye == true)
		{
			A.SetBool("IsEye",true);
			SR.sprite = EnemyEyeSprite;
		
		}

		if(StateEnemyChoice.enemyattacked == true)
		{
		
			A.SetTrigger("DidEnemyAttack");
			

			
		}


		if(StatePlayerChoice.EnemyDead == true)
		{
			StatePlayerChoice.EnemyDead = false;

			GameObject.Destroy(gameObject);
			ParticleEffects.Instance.Explosion(transform.position);
		}
	


	}
}
