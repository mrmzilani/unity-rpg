﻿using UnityEngine;
using System.Collections;

public class PlayerBattleSprites : MonoBehaviour {


	public Sprite WarriorSprite;
	public Sprite MageSprite;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		SpriteRenderer SR = GetComponent<SpriteRenderer>();
		
		if (SR.sprite == null) // if the sprite on spriteRenderer is null then
		{
			//SR.sprite = WarriorSprite; // set the sprite to sprite1
		}
		
		if (CreateNewCharacter.isMageClass == true) // if the sprite on spriteRenderer is null then
		{
			SR.sprite = MageSprite; // set the sprite to sprite1
		}
		
		if (CreateNewCharacter.isWarriorClass == true) // if the sprite on spriteRenderer is null then
		{
			SR.sprite = WarriorSprite; // set the sprite to sprite1
		}
		
		
		if(StateEnemyChoice.playerDead == true)
		{
			StateEnemyChoice.playerDead = false;
			GameObject.Destroy(gameObject);
			ParticleEffects.Instance.Explosion(transform.position);
		}
		
	
	}
}
