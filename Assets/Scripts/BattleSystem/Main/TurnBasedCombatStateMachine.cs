﻿using UnityEngine;
using System.Collections;

public class TurnBasedCombatStateMachine : MonoBehaviour {



	public Sprite WarriorSprite;
	public Sprite MageSprite;

	public GameObject battlewindow;

//	private StateStart gamestart = new StateStart ();
//	private StatePlayerChoice playerchoose = new StatePlayerChoice ();
//	private StateEnemyChoice enemychoose = new StateEnemyChoice();

	public enum BattleStates //States
	{
		START,
		PLAYERCHOICE,
		ENEMYCHOICE,
		CALCULATE,
		ANIMATESCENE,
		LOSE,
		WIN,
	}

	

	public static BattleStates currentState; //The State
	
	void Awake()
	{
	
		
	}
	
	// Use this for initialization
	void Start () {

		currentState = BattleStates.START;
	
		

		
		bool xpAdded = false;
		
		
		battlewindow.gameObject.guiText.text = "START";
		


	
		}
	
	// Update is called once per frame
	void Update () {

		#region States

		switch (currentState) {


		case(BattleStates.START):
			Debug.Log("Start");
			currentState = BattleStates.PLAYERCHOICE;
			break;

		case(BattleStates.PLAYERCHOICE):
			Debug.Log("Player move");

			break;

			case(BattleStates.ENEMYCHOICE):
			Debug.Log("Enemy move");
			break;


		case(BattleStates.LOSE):
			Debug.Log("Lost");
			break;


		case(BattleStates.WIN):
		
			Debug.Log("Won");
			
		

			
			break;

		}

		#endregion
		
		#region SetBattleWindowInfo

		if(currentState == BattleStates.START)
		{
			battlewindow.gameObject.guiText.text = "START";
		}
		
		
		if(currentState == BattleStates.PLAYERCHOICE)
		{
			battlewindow.gameObject.guiText.text = "PLAYER CHOICE";
		}
		


		if(currentState == BattleStates.ENEMYCHOICE)
		{
			battlewindow.gameObject.guiText.text = "ENEMY CHOICE";
		}
			
			
		if(currentState == BattleStates.WIN)
		{
			battlewindow.gameObject.guiText.text = "WIN";
		}
		
		if(currentState == BattleStates.LOSE)
		{
			battlewindow.gameObject.guiText.text = "YOU DIED";
		}
		
		
		#endregion


	

	}



	void OnGUI()
	{
	
		//Displays Players Info
		GUI.Box(new Rect(Screen.width-200,Screen.height-32,200,32),GameInformation.PlayerName + "  " + GameInformation.PlayerStamina + "  HP  /  " + GameInformation.PlayerMP + " MP");
	
		GUI.Box(new Rect(0,Screen.height-32,200,32),EnemyInformation.EnemyName + "  " +  EnemyInformation.EnemyStamina + "  HP  /  " +  EnemyInformation.EnemyMP  + " MP"); 
		        
	
		Debug.ClearDeveloperConsole();



	}
	

	

}