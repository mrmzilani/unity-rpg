﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour {


	public GameObject thePlayer;

	Animator A;

	public Sprite WarriorSprite;
	public Sprite MageSprite;
	
	public float playerSpeed = 4;
	private static bool created = false;

	private string LastScene;
	public static string UpdateLastScene;


	void Awake () {
	//http://docs.unity3d.com/ScriptReference/Object.DontDestroyOnLoad.html
	

	if (!created) 
		{
			DontDestroyOnLoad (transform.gameObject);
			created = true;
				
		} 
		else 
		{
			Destroy(this.gameObject); 
		}


	}



	// Use this for initialization
	void Start () {

	
		A = GetComponent<Animator>();


	}
	
	// Update is called once per frame
	void Update () {
	
	
		UpdateLastScene = Application.loadedLevelName;
	
		if(GameInformation.Invisable == false) //if not in a battle
		{
			Movement(); //Allow movement
		}
		
//		ChangePlayerSprite();
		KeyboardActions();
		
	}

	
	void KeyboardActions()
	{
		if(Application.loadedLevelName == "WorldMapScene")
		{
			if (Input.GetKey (KeyCode.Return)) 
			{
				LastScene = "WorldMapScene";
				GameInformation.Invisable = true;
				Application.LoadLevel ("PauseMenu"); 
			}
		
		}
		
		if(Application.loadedLevelName == "Town")
		{
			if (Input.GetKey (KeyCode.Return)) 
			{
				LastScene = "Town";
				GameInformation.Invisable = true;
				Application.LoadLevel ("PauseMenu"); 
			}
			
		}
		
		if(Application.loadedLevelName == "PauseMenu")
		{
			if (Input.GetKey (KeyCode.Return)) 
			{
				GameInformation.Invisable = false;
				Application.LoadLevel (LastScene); 
			}
			
		}
	
	}


	void Movement()
	{
		
		//get key - long press 
		//get key down - one press (double jump)
		

			
			if (Input.GetKey (KeyCode.D)) 
			{
			
			
				transform.Translate (Vector2.right * playerSpeed * Time.deltaTime);
				
				A.SetBool("RightKeyPressed",true);
				
			}
			else
			{
				A.SetBool("RightKeyPressed",false);
			}
			
			
			if (Input.GetKey (KeyCode.A)) 
			{
				
				transform.Translate (-Vector2.right * playerSpeed * Time.deltaTime);
				A.SetBool("LeftKeyPressed",true);
				
			}
			else
			{
				A.SetBool("LeftKeyPressed",false);
			}
			
			
			
			

			if (Input.GetKey (KeyCode.S)) 
			{
			
				transform.Translate (new Vector2(0,-1) * playerSpeed * Time.deltaTime);
				A.SetBool("DownKeyPressed",true);
			
			}
			else
			{
				A.SetBool("DownKeyPressed",false);
			}
			

			if (Input.GetKey (KeyCode.W)) 
			{
				transform.Translate (new Vector2(0,1) * playerSpeed * Time.deltaTime);
				A.SetBool("UpKeyPressed",true);
			
			}
			else
			{
				A.SetBool("UpKeyPressed",false);
			}
		
			
			
		if (Input.GetKey (KeyCode.Space)) 
		{
			playerSpeed = 7;
		}
		else
		{
			playerSpeed = 4;
		
		}
		

			
			
			
	}
	
	void OnLevelWasLoaded() 
	{
		
		
		if(Application.loadedLevelName == "Town")
		{
			
		 	transform.position = new Vector2(-6,-10);
		}
		
		if(Application.loadedLevelName == "WorldMapScene" && UpdateLastScene == "Town")
		{

			transform.position = new Vector2(0,13);
			
		}
		
		
		
	}


/*	void ChangePlayerSprite()
	{
	
		SpriteRenderer SR = GetComponent<SpriteRenderer>();
		
		if (SR.sprite == null) // if the sprite on spriteRenderer is null then
		{
			SR.sprite = WarriorSprite; // set the sprite to sprite1
		}
		
		if (CreateNewCharacter.isMageClass == true) // if the sprite on spriteRenderer is null then
		{
			SR.sprite = MageSprite; // set the sprite to sprite1
		}
		
		if (CreateNewCharacter.isWarriorClass == true) // if the sprite on spriteRenderer is null then
		{
			SR.sprite = WarriorSprite; // set the sprite to sprite1
		}
		
		
		//http://gamedev.stackexchange.com/questions/72765/change-the-sprite-of-a-object-in-unity
		
	}	*/
	



}
