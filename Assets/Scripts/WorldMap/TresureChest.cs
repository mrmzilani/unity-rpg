﻿using UnityEngine;
using System.Collections;

public class TresureChest : MonoBehaviour {


	//http://unity3d.com/learn/tutorials/modules/beginner/2d/2d-controllers
	
	
	Animator A;
	bool isOpen;
	public GameObject TheSprite;
	
	private static bool created = false;
	
	void Awake () {
		
	/*	if (!created) 
		{
			DontDestroyOnLoad (transform.gameObject);
			created = true;
			
		} 
		else 
		{
			
			Destroy(this.gameObject); 
		
		}*/
		
		
	}
	
	
	// Use this for initialization
	void Start () {
	
		isOpen = false;
		
		A = GetComponent<Animator>();
		
	
	
	}
	
	// Update is called once per frame
	void Update () {
	
	/*	if(GameInformation.Invisable == true)
		{
			this.renderer.enabled = false;
		}
		else
		{
			this.renderer.enabled = true;
		}*/
		
	
	}
	
	void OnCollisionEnter2D(Collision2D col)
	{
		if(col.gameObject.tag == "Player")
		{
		
			if(isOpen == false)
			{
				//Debug.Log("chest has been open");
				A.SetBool("Open",true);
				
				GameInformation.theGold += 100;
				
				isOpen = true; //cant be opened twice;
		
	
			} 
		
		}
	
	}
	


	
	public void Reset()
	{
		A.SetBool("Open",false);
		isOpen = false; 
		
	}
	
	
}

