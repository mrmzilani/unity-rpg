﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {
	//https://www.youtube.com/watch?v=hVLqLX4Yups


	public Transform player;
	
	public float smoothrate = 0.5f;

	private Transform thisTransform;
	private Vector2 velocity;	
	
	public static bool created = false;

	void Awake () {
		//http://answers.unity3d.com/questions/34185/dontdestroyonload-is-it-intended-behavior.html
		
		
		if (!created) 
		{
			DontDestroyOnLoad (transform.gameObject);
			created = true;
			
		} 
		else 
		{
			Destroy(this.gameObject); 
		}
		
	
		
	}


	// Use this for initialization
	void Start () {
		
		
	
		
		
		thisTransform = transform;
		
		velocity = new Vector2 (0.5f, 0.5f);
		
	}
	
	// Update is called once per frame
	void Update () {
		
		
	
		Vector2 newPos2D = Vector2.zero;

		
		newPos2D.x = Mathf.SmoothDamp (thisTransform.position.x, 
		                               player.position.x, ref velocity.x, smoothrate);
		
		
		
		newPos2D.y = Mathf.SmoothDamp (thisTransform.position.y, 
		                               player.position.y, ref velocity.y, smoothrate);
		
		//Update camera to player location
		
		
		Vector3 newPos = new Vector3 (newPos2D.x, newPos2D.y, transform.position.z);
		transform.position = Vector3.Slerp (transform.position, newPos, Time.time);
		
	
		
		
	}
	
	void OnLevelWasLoaded() 
	{
		
		
		if(Application.loadedLevelName == "Town")
		{
			transform.position = new Vector3(-6,-10,-10);
			
		}
		
		if(Application.loadedLevelName == "WorldMapScene" && PlayerScript.UpdateLastScene == "Town")
		{
			
			transform.position = new Vector3(0,13,-10);
			
		}
		
		
		
	}

	
}
