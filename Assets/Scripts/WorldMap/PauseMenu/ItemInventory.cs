﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


//https://www.youtube.com/watch?v=WfvjA8mullw 

public class ItemInventory : MonoBehaviour {


	//COMBO BOX
	public List<string> itemString = new List<string>();


	public Rect Box;
	
	private string selectedItem = "Select Item";
	
	private bool editing = false; //checks if box has been pressed

	///ADDING POTIONS
	
	private BasePotion addAPotion;
	
	private CreateNewPotion getPotionInfo = new CreateNewPotion();

	
	private BasePotion LoadPotion;

	public int potioncount;
	
	public bool itemIsSelected;
	public int currentlySelectedItem = 0;
	
	// Use this for initialization
	void Awake()
	{
		

		//CreateLoadedPotions();	
	
	
		
	}
	
	void CreateLoadedPotions()
	{
		if(GameInformation.LoadPotionsOnce == true)
		{
			LoadInformation.LoadPotionInfomation();
			
			LoadPotion = new BasePotion();
						
			
			////
			
			getPotionInfo.CreateSpecificPotion(LoadPotion,GameInformation.PotionSlot0);
			
			Debug.Log (LoadPotion.theItemName + "was loaded");
			
			GameInformation.ListofPotion.Insert(0,LoadPotion);
			
			////
			
			getPotionInfo.CreateSpecificPotion(LoadPotion,GameInformation.PotionSlot1);
			
			Debug.Log (LoadPotion.theItemName + "was loaded");
			
			GameInformation.ListofPotion.Insert(1,LoadPotion);
			
			
			GameInformation.LoadPotionsOnce = false;
			
		}	
	
	
	}
	
	
	void Start () {	
		
		
		//get the names of each item which is in the potions list	
		for( int x = 0; x < GameInformation.ListofPotion.Count; x++)
		{
			itemString.Add(GameInformation.ListofPotion[x].theItemName);
		}
		
		//make the potion counter the same amount as in the list
		potioncount = GameInformation.ListofPotion.Count;


	}
	
	// Update is called once per frame
	void Update () {

		
	
	}
	
	
	
	private void OnGUI()
	{
	
		
		for( int x = 0; x < GameInformation.ListofPotion.Count && x < itemString.Count; x++)
		{
			itemString[x] = GameInformation.ListofPotion[x].theItemName;
		}
	
	
		if(GUI.Button (new Rect (200, 0, 80, 32), "Back"))
		{	
			//SaveInformation.SavePotionInfomation();
			Application.LoadLevel("PauseMenu"); 
		}
		
	
		
		if(GUI.Button(new Rect(Box.x,Box.y,200,30),selectedItem))
		{
			editing = true;
			itemIsSelected = false;
		}
		
		if(editing)
		{
			for(int x = 0 ; x < itemString.Count; x++) //creates buttons for each item
			{
				
				if(GUI.Button(new Rect(Box.x, (Box.height * x) + Box.y +30 ,Box.width,Box.height), itemString[x]))
				{
					selectedItem = itemString[x];
					currentlySelectedItem = x;
					editing = false;
					itemIsSelected = true;
				}
			
			}
		
		}
		
		
		if(GUI.Button(new Rect(400,30,200,30),"Create Potion"))
		{
			addAPotion = new BasePotion(); // create an empty potion
			
			getPotionInfo.CreatePotion(addAPotion); //fill up empty potion with properties
		
			GameInformation.ListofPotion.Add(addAPotion); //add to the list of potions
			
			//LOGpotion(); //output to console
			
			itemString.Add(GameInformation.ListofPotion[potioncount].theItemName); //updates the combobox string
			potioncount+=1; //adds one to reach next element
			
		
		
			
		}
	

		//ANY BUTTON PAST THIS LINE CAN BE DISABLED BY ITEM SELECTION
			
		if(itemIsSelected == false)
		{
			GUI.enabled = false;
		}

		
		if(GUI.Button(new Rect(400,100,200,30),"Discard Potion"))
		{			
			if(potioncount >= 1)
			{
			potioncount -= 1; //subtracts one from total count
			selectedItem = "Select Potion"; //resets label
			
			//removes the currently selected item in the list of potions
		
	
			PlayerPrefs.SetInt("P"+currentlySelectedItem.ToString(),7);
			Debug.Log("P"+currentlySelectedItem.ToString());
	
			GameInformation.ListofPotion.RemoveAt(currentlySelectedItem);
			itemString.RemoveAt(currentlySelectedItem);
			}
			
			currentlySelectedItem = 0;
			itemIsSelected = false;
		
		}
	
		
		if(GUI.Button(new Rect(400,140,200,30),"Use Potion"))
		{
		
			if(potioncount >= 0)
			{
			
			#region USE POTION TYPE
			if(GameInformation.ListofPotion[currentlySelectedItem].thePotionType == BasePotion.PotionTypes.HEALTH)
			{
					GameInformation.PlayerStamina += GameInformation.ListofPotion[currentlySelectedItem].ThePotionValue; //add potion value to the stats 
			}
			
			if(GameInformation.ListofPotion[currentlySelectedItem].thePotionType == BasePotion.PotionTypes.MP)
			{
					GameInformation.PlayerMP += GameInformation.ListofPotion[currentlySelectedItem].ThePotionValue; //add potion value to the stats 
			}
		
			if(GameInformation.ListofPotion[currentlySelectedItem].thePotionType == BasePotion.PotionTypes.STRENGTH)
			{
					GameInformation.PlayerStrength += GameInformation.ListofPotion[currentlySelectedItem].ThePotionValue; //add potion value to the stats 
			}
				
			if(GameInformation.ListofPotion[currentlySelectedItem].thePotionType == BasePotion.PotionTypes.INTELLECT)
			{
					GameInformation.PlayerIntellect += GameInformation.ListofPotion[currentlySelectedItem].ThePotionValue; //add potion value to the stats 
			}
				
			if(GameInformation.ListofPotion[currentlySelectedItem].thePotionType == BasePotion.PotionTypes.SPEED)
			{
					GameInformation.PlayerSpeed += GameInformation.ListofPotion[currentlySelectedItem].ThePotionValue; //add potion value to the stats 
			}
				
			if(GameInformation.ListofPotion[currentlySelectedItem].thePotionType == BasePotion.PotionTypes.LUCK)
			{
					GameInformation.PlayerLuck += GameInformation.ListofPotion[currentlySelectedItem].ThePotionValue; //add potion value to the stats 
			}
			#endregion
			
			
			potioncount -= 1; //subtracts one from total count
			
			selectedItem = "Select Potion"; //resets label
	
			
			//removes the currently selected item in the list of potions
			GameInformation.ListofPotion.RemoveAt(currentlySelectedItem);
			itemString.RemoveAt(currentlySelectedItem);
			
			currentlySelectedItem = 0;
			itemIsSelected = false;
			
			}
			
		}
		
		if(GUI.Button(new Rect(400,180,200,30),GameInformation.PlayerName + "  " + GameInformation.PlayerStamina + "  HP  /  " + GameInformation.PlayerMP + " MP"))
		{
			
		 
	
		}
		

	
	
	}
	
	void LOGpotion()
	{
		Debug.Log ("THE POTION LIST");
		
		for(int x = 0; x < GameInformation.ListofPotion.Count; x++)
		{
			Debug.Log (GameInformation.ListofPotion[x].theItemName);
			Debug.Log (GameInformation.ListofPotion[x].theItemID);
			Debug.Log (GameInformation.ListofPotion[x].theItemDescription);
			Debug.Log (GameInformation.ListofPotion[x].thePotionType);
		}
	}
	
}
