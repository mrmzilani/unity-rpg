﻿using UnityEngine;
using System.Collections;

public class PauseScript : MonoBehaviour {


	public Rect Box;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		//GameInformation.Invisable = true;
	
	}
	
	void OnGUI()
	{
		
		if(GUI.Button (new Rect (0, 0, 80, 32), "Items"))
		{
			Application.LoadLevel("ItemsInventoryMenu"); 
		}
		
		if(GUI.Button (new Rect (0, 32, 80, 32), "Equipment"))
		{
			Application.LoadLevel("EquipmentMenu"); 
		}
		
		if(GUI.Button (new Rect (0,64, 80, 32), "Weapon"))
		{
			Application.LoadLevel("WeaponMenu"); 
		}
		
		if(GUI.Button (new Rect (0,96, 80, 32), "Quest"))
		{
			Application.LoadLevel("QuestMenu"); 
		}
		
		if(GUI.Button (new Rect (0,128, 80, 32), "Quit"))
		{
			GameInformation.Invisable = true; //Battle is ended player exited battle
			Application.LoadLevel("MainMenu"); 
		}
		

		
		GUI.Box(new Rect(200,0,200,32),GameInformation.PlayerName + "  " + GameInformation.PlayerStamina + "  HP  /  " + GameInformation.PlayerMP + " MP");
		GUI.Box(new Rect(200,32,200,32),GameInformation.PlayerLevel + " Level");
		GUI.Box(new Rect(200,64,200,32),GameInformation.PlayerStrength + " Strength");
		GUI.Box(new Rect(200,96,200,32),GameInformation.PlayerIntellect+ " Intellect");
		GUI.Box(new Rect(200,128,200,32),GameInformation.PlayerSpeed+ " Speed");
		GUI.Box(new Rect(200,160,200,32),GameInformation.PlayerLuck+ " Luck");
		
		GUI.Box(new Rect(200,192,200,32),GameInformation.theCurrentXP + " currentXP");
		GUI.Box(new Rect(200,224,200,32),GameInformation.theRequiredXP+ " RequiredXP");
		
		GUI.Box(new Rect(200,256,200,32),GameInformation.theGold+ " Gold");
		
		
		//if(GUI.Button(new Rect(Box.x,Box.y,200,200),"Player Stats"))
		//{
			
		//}
		
		
	}
}
