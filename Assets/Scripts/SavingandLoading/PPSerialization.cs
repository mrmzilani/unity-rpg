﻿using UnityEngine;
using System.Collections;

using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;



public class PPSerialization {

	public static BinaryFormatter binaryFormatter = new BinaryFormatter();


	public static void Save(string saveTag, object obj) //save anywhere
	{
		MemoryStream memoryStream = new MemoryStream();

		binaryFormatter.Serialize(memoryStream,obj);

		string temp = System.Convert.ToBase64String(memoryStream.ToArray());

		//Formats an object onto binary

		PlayerPrefs.SetString(saveTag,temp);

		//Saves in player prefs


		
	}


	public static object Load(string saveTag)
	{
		string temp = PlayerPrefs.GetString(saveTag); //get the string
		
		if(temp == string.Empty) //if empty do nothing
		{
			return null;
		}
		
		MemoryStream memoryStream = new MemoryStream(System.Convert.FromBase64String(temp));
		//send the stream the temp string
		
		 return binaryFormatter.Deserialize(memoryStream);
		// return the object 
		
	}
	
	
	



}
